package com.nuospin.springrestful;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.nuospin.springrestful.domain.User;
import com.nuospin.springrestful.dto.UserDto;
import com.nuospin.springrestful.service.UserService;

public class UserServiceTest extends ApplicationTest {
	
	@Autowired
	private UserService userService;

	@Test
	public void shouldBeableToSaveUser(){
		UserDto user = buildUser();
		User registeredUser = userService.saveUser(user);
		System.out.println("user" + registeredUser);
		Assert.notNull(registeredUser);
		Assert.isTrue(registeredUser.getEmail().equals("pooja@nuospin.com"));

	}
	


	private UserDto buildUser() {
		UserDto user = new UserDto();
		user.setEmail("pooja@nuospin.com");
		user.setFirstName("pooja");
		user.setLastName("mehra");
		return user;

	}

}