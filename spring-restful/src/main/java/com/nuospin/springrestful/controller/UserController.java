package com.nuospin.springrestful.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nuospin.springrestful.domain.User;
import com.nuospin.springrestful.dto.UserDto;
import com.nuospin.springrestful.service.UserService;
import com.nuospin.springrestful.utill.RestResponse;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = { "/{firstName}" }, method = RequestMethod.GET)
	public RestResponse<User> getUserByFirstName(@PathVariable String firstName) {

		User user = userService.getUserByFirstName(firstName);
		return new RestResponse<User>(true, null, user);

	}

	@RequestMapping(value = { "/save" }, method = RequestMethod.POST)
	public RestResponse<User> saveUser(@RequestBody UserDto userDto) {

		User user = userService.saveUser(userDto);
		return new RestResponse<User>(true, null, user);

	}
	
	@RequestMapping(value = { "/update/{userId}" }, method = RequestMethod.PUT)
	public RestResponse<User> updateUser(@PathVariable String userId, @RequestBody UserDto userDto) {

		User user = userService.updateUser(userId, userDto);
		return new RestResponse<User>(true, null, user);

	}
	
	@RequestMapping(value = { "/delete/{userId}" }, method = RequestMethod.DELETE)
	public RestResponse<User> deleteUser(@PathVariable String userId) {

		userService.deleteUser(userId);
		return new RestResponse<User>(true, null, null);

	}
	

}
