package com.nuospin.springrestful.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuospin.springrestful.domain.User;
import com.nuospin.springrestful.dto.UserDto;
import com.nuospin.springrestful.repository.UserRepository;
import com.nuospin.springrestful.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User getUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public User saveUser(UserDto userDto) {
		User user = getUserByEmail(userDto.getEmail());
		if (user == null) {
			user = new User();
			BeanUtils.copyProperties(userDto, user);
			return userRepository.save(user);
		}
		return null;
	}

	@Override
	public User updateUser(String id, UserDto userDto) {
		User user = userRepository.findOne(id);
		user.setFirstName((userDto.getFirstName() == null) ? user.getFirstName() : userDto.getFirstName());
		user.setLastName((userDto.getLastName() == null) ? user.getLastName() : userDto.getLastName());
		user.setEmail((userDto.getEmail() == null) ? user.getEmail() : userDto.getEmail());
		user.setContactNumber((userDto.getContactNumber() == null) ? user.getContactNumber() : userDto.getContactNumber());
		return userRepository.save(user);
	}

	@Override
	public void deleteUser(String id) {
		userRepository.delete(id);
	}

	@Override
	public User getUserByFirstName(String firstName) {
		return userRepository.findByfirstName(firstName);
	}
	


}
