package com.nuospin.springrestful.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nuospin.springrestful.domain.User;

public interface UserRepository extends MongoRepository<User, String> {
	
	 User findByEmail(String email);
	 User findByfirstName(String firstName);

}
