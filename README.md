# README #


### What is this repository for? ###

Sample RESTful APIs with Spring Boot and MongoDb.


### How do I get set up? ###

1 - Install Java 8, Maven 3.

2- Clone the repository.

3- cd springrestfulapis/spring-restful

4- Run command "mvn clean install" to clean and install required maven packages.

5- Run command "mvn spring-boot:run" to run the server. It will start your sever on localhost:9090.

6- Type localhost:9090 on browser. you will see welcome message on screen.




### Who do I talk to? ###

pooja@nuospin.com